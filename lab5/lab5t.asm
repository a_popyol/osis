.model small
.stack 1000
.data
old dd 0 
counter db 0
flag db 0
.code
.486


new_handle proc        
    pusha       
    
    mov flag, 0
    inc counter
    cmp counter, 20
    jne old_handler
    
    xor cl, cl
    mov counter, cl
    mov flag, 1
    
old_handler: 
    popa                    
    jmp dword ptr cs:old       
    xor ax, ax
    mov al, 20h
    out 20h, al 
    jmp exit
        
exit:
    xor ax, ax
    mov al, 20h
    out 20h, al 
    popa
    mov cl, flag
    iret
new_handle endp
new_end:



main:
    mov ax, @data
    mov ds, ax
        
    cli
    pushf 
    push 0      
    pop ds
    mov eax, ds:[08h*4] 
    mov cs:[old], eax 
        
    mov ax, cs
    shl eax, 16
    mov ax, offset new_handle
    mov ds:[08h*4], eax
    sti 
        
    xor ax, ax
    mov ah, 31h
    MOV DX, (New_end - @code + 10FH) / 16 
    INT 21H 
end main
