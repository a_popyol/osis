.model    small
.stack    100h
.386
.data

 buffer db 254,0,254 dup (0)  
 outfile db 254,0,254 dup (0)  
 
    endline   db 10,13,'$'
    qqq db '*'
    qqqq db 10,13,'kek',10,13,'$'
    dta db 254 dup (?)
    read db 254 dup ('2')
    input dw 1
    output dw 1
.code


main    proc

    push    @data
    pop    ds
    
    mov ah,0ah             
    mov dx, offset buffer          ;read input name
    int 21h     
    mov al,10
    int 29h
    
    mov dx, offset endline
    mov ah, 09h
    int 21h
    
    mov ah,0ah             
    mov dx, offset outfile          ;read output name
    int 21h 
    mov al,10
    int 29h
    
    xor cx, cx
    lea bx, outfile + 2
    mov cl, outfile + 1
    add bx, cx
    mov byte ptr [bx], '$'
    mov ah,3Ch
    mov dx, offset outfile + 2
    xor cx,cx
    int 21h
    jnc CreateErrorSkip

lea dx, endline
mov ah, 09h
int 21h

add al, '0'
mov dl, al  
mov ah, 02h
int 21h
jmp ex


CreateErrorSkip:
    mov output, ax
    
    xor bx, bx
    mov bl, buffer[1]
    mov si, bx
    add si, 2
    mov dh, qqq
    mov buffer[si], dh
    add si, 1
    mov dl, '.'
    mov buffer[si], dl
    add si, 1
     mov buffer[si], dh
    mov dh, 0
    add si, 1
    mov buffer[si], dh   ; create input template
    
    mov ah, 1ah
    lea dx, dta   ; set dta 
    int 21h
    
    mov ah, 4eh
    xor cx, cx
    lea dx, buffer[2]  ; find first input
    int 21h
    jmp lp
    
    ylp:
    mov ah, 4fh
    xor cx, cx
    lea dx, buffer[2] ; find next input
    int 21h
    
    lp:
    jc ex
    
    mov si, 2ah
    mov dta[si], 0
    
    mov si, 1eh
    lea dx, dta[si] 
    mov ah, 3dh
    xor al, al
    int 21h
    mov input, ax  ; open input file
    
ra:
    mov bx, [input] ; read input file
    mov ah, 3fh
    lea dx, read
    mov cx, 0100h
    int 21h
    
    mov cx, ax
    mov bx, [output]
    mov ah, 40h
    lea dx, read ; write output file
    int 21h
    
    cmp ax, 0100h
    jz ra
    
    mov ah, 3eh
    mov bx, [input]   ; close input file
    int 21h
    
    jmp ylp
    
ex:
    mov ah, 3eh
    mov bx, [output]  ; close output file
    int 21h
    
    mov    ax,    04C00h
    int    21h

main    endp

end    main