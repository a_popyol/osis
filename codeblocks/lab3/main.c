#include <stdio.h>
#include <stdlib.h>


char ** strings;
int n = 1;
int length = 0;

void executeDouble() {
    n = n * 2;
    char ** newStrings = (char **) malloc(n * sizeof(char*));
    for(int i=0;i<length;i++) {
        newStrings[i] = strings[i];
    }
    free(strings);
    strings = newStrings;
}

int main()
{
    int index;
    strings = (char **) malloc(n * sizeof(char*));
    while (1) {
        int operation;
        char * str = (char*) malloc(85);
        scanf("%d", &operation);
        switch(operation){
            case 1:
                scanf("%s", str);
                strings[length] = str;
                length += 1;
                if(length == n) {
                    executeDouble();
                }
                break;
            case 2:
                scanf("%d %s", &index, str);
                if(index>=length){
                    break;
                }
                free(strings[index]);
                strings[index] = str;
                break;
            case 3:
                scanf("%d", &index);
                if(index>=length){
                    break;
                }
                free(strings[index]);
                for(int i=index;i<length-1;i++){
                    strings[i] = strings[i+1];
                }
                length -= 1;
                break;
            case 4:
                for(int i=0;i<length;i++)
                    printf("%s\n", strings[i]);
                break;
            case 5:
                return 0;
        }
    }
    return 0;
}
