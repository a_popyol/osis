﻿// lab10.1.cpp : Определяет точку входа для приложения.
//

#include "framework.h"
#include "lab10.1.h"

#define MAX_LOADSTRING 100

// Глобальные переменные:
HINSTANCE hInst;                                // текущий экземпляр
WCHAR szTitle[MAX_LOADSTRING];                  // Текст строки заголовка
WCHAR szWindowClass[MAX_LOADSTRING];            // имя класса главного окна

// Отправить объявления функций, включенных в этот модуль кода:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

HWND Another_Wnd;

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Разместите код здесь.

    // Инициализация глобальных строк
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_LAB101, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Выполнить инициализацию приложения:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_LAB101));

    MSG msg;

    // Цикл основного сообщения:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  ФУНКЦИЯ: MyRegisterClass()
//
//  ЦЕЛЬ: Регистрирует класс окна.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_LAB101));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_LAB101);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

#define RBUTTON1_ID 1001
#define RBUTTON2_ID 1002
#define RBUTTON3_ID 1003

#define RBUTTON4_ID 1004
#define RBUTTON5_ID 1005
#define RBUTTON6_ID 1006
#define RBUTTON7_ID 1007

#define CHECKBOX_ID 1008

#define DRAW_MESSAGE "DrawMessage"
#define COLOR_MESSAGE "ColorMessage"
#define SHAPE_MESSAGE "ShapeMessage"

UINT WM_DRAW;
UINT WM_COLOR;
UINT WM_SHAPE;

//
//   ФУНКЦИЯ: InitInstance(HINSTANCE, int)
//
//   ЦЕЛЬ: Сохраняет маркер экземпляра и создает главное окно
//
//   КОММЕНТАРИИ:
//
//        В этой функции маркер экземпляра сохраняется в глобальной переменной, а также
//        создается и выводится главное окно программы.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Сохранить маркер экземпляра в глобальной переменной

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   CreateWindow(L"button", L"Red",
	   WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON,
	   10, 10, 80, 30, hWnd, (HMENU)RBUTTON1_ID, hInstance, NULL);
   CreateWindow(L"button", L"Green",
	   WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON,
	   10, 35, 80, 30, hWnd, (HMENU)RBUTTON2_ID, hInstance, NULL);
   CreateWindow(L"button", L"Blue",
	   WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON,
	   10, 60, 80, 30, hWnd, (HMENU)RBUTTON3_ID, hInstance, NULL);

   CreateWindow(L"button", L"Rhombus",
	   WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON,
	   110, 10, 80, 30, hWnd, (HMENU)RBUTTON4_ID, hInstance, NULL);
   CreateWindow(L"button", L"Square",
	   WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON,
	   110, 35, 80, 30, hWnd, (HMENU)RBUTTON5_ID, hInstance, NULL);
   CreateWindow(L"button", L"Circle",
	   WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON,
	   110, 60, 80, 30, hWnd, (HMENU)RBUTTON6_ID, hInstance, NULL);
   CreateWindow(L"button", L"Star",
	   WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON,
	   110, 85, 80, 30, hWnd, (HMENU)RBUTTON7_ID, hInstance, NULL);

   CheckRadioButton(hWnd, RBUTTON1_ID, RBUTTON3_ID, RBUTTON1_ID);
   CheckRadioButton(hWnd, RBUTTON4_ID, RBUTTON7_ID, RBUTTON4_ID);

   CreateWindow(L"button", L"Draw",
	   WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX,
	   10, 150, 80, 30, hWnd, (HMENU)CHECKBOX_ID, hInstance, NULL);

   WM_DRAW = RegisterWindowMessage((LPCWSTR)DRAW_MESSAGE);
   WM_COLOR = RegisterWindowMessage((LPCWSTR)COLOR_MESSAGE);
   WM_SHAPE = RegisterWindowMessage((LPCWSTR)SHAPE_MESSAGE);

   Another_Wnd = FindWindow(NULL, L"Lab10.2");

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  ФУНКЦИЯ: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  ЦЕЛЬ: Обрабатывает сообщения в главном окне.
//
//  WM_COMMAND  - обработать меню приложения
//  WM_PAINT    - Отрисовка главного окна
//  WM_DESTROY  - отправить сообщение о выходе и вернуться
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	Another_Wnd = FindWindow(NULL, L"Lab10.2");

    switch (message)
    {
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
            // Разобрать выбор в меню:

			if (wmId >= RBUTTON1_ID && wmId <= RBUTTON3_ID) {
				SendMessage(Another_Wnd, WM_COLOR, 0, wmId - RBUTTON1_ID);

				CheckRadioButton(hWnd, RBUTTON1_ID, RBUTTON3_ID, wmId);
			}
			else if (wmId >= RBUTTON4_ID && wmId <= RBUTTON7_ID) {
				SendMessage(Another_Wnd, WM_SHAPE, 0, wmId - RBUTTON4_ID);

				CheckRadioButton(hWnd, RBUTTON4_ID, RBUTTON7_ID, wmId);
			}
            else switch (wmId)
            {
			case CHECKBOX_ID:
			{
				SendMessage(Another_Wnd, WM_DRAW, 0, 0);

				HWND hWndCheck = GetDlgItem(hWnd, CHECKBOX_ID);
				LRESULT state = SendMessage(hWndCheck, BM_GETCHECK, 0, 0);
				if (state == BST_CHECKED)
				{
					SendMessage(Another_Wnd, WM_DRAW, 0, 1);
				}
				if (state == BST_UNCHECKED)
				{
					SendMessage(Another_Wnd, WM_DRAW, 0, 0);
				}
			}
			break;
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Добавьте сюда любой код прорисовки, использующий HDC...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Обработчик сообщений для окна "О программе".
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
