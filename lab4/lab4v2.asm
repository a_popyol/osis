.model small
.stack 1000
.data
old dd 0 
.code
.486


new_handle proc        
    pusha       
    xor ax, ax 
    in al, 60h        
    cmp al, 12h        
    je old_handler
    cmp al, 15h       
    je old_handler
    cmp al, 16h       
    je old_handler
    cmp al, 17h        
    je old_handler
    cmp al, 18h      
    je old_handler
    cmp al, 1Eh     
    je old_handler
           
    mov BX, 1
    xor DX, DX
    div BX
    cmp DX, 0
    je exit
    
    old_handler: 
        popa                    
        jmp dword ptr cs:old       
        xor ax, ax
        mov al, 20h
        out 20h, al 
        jmp exit
        
     exit:
        xor ax, ax
        mov al, 20h
        out 20h, al 
        popa
    iret
new_handle endp
new_end:



main:
    mov ax, @data
    mov ds, ax
        
    cli
    pushf 
    push 0      
    pop ds
    mov eax, ds:[09h*4] 
    mov cs:[old], eax 
        
    mov ax, cs
    shl eax, 16
    mov ax, offset new_handle
    mov ds:[09h*4], eax
    sti 
        
    xor ax, ax
    mov ah, 31h
    MOV DX, (New_end - @code + 10FH) / 16 
    INT 21H 
end main
