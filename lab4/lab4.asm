.MODEL SMALL
.STACK  256
.DATA
.CODE
New21   PROC
    cmp ah, 10
    je  @@change
    db  0EAh    
Old dw  0, 0    
@@change:
    push    ds
    push    dx
    push    cs
    pop ds
    jmp cycle
    cycleprint:
        mov dl, al
        mov ah, 2
        int 21h
    cycle:
        mov ah, 8
        int 21h
        cmp al, 13
        jz endcycle
        cmp al, 'a'
        jz cycleprint
        cmp al, 'e'
        jz cycleprint
        cmp al, 'u'
        jz cycleprint
        cmp al, 'y'
        jz cycleprint
        cmp al, 'i'
        jz cycleprint
        cmp al, 'o'
        jz cycleprint  
        jmp cycle
    endcycle:
    ;=================
    ;pushf
    ;call    dword ptr [Old] 
    pop dx
    pop ds
    iret
New21   ENDP
Init    PROC
 
    mov ax, 3521h
    int 21h
    mov cs:[Old], bx
    mov cs:[Old+2], es
 
    push    cs
    pop ds
    lea dx, New21
    mov ax, 2521h
    int 21h
 
    lea dx, Init+15 
    shr dx, 1       
    shr dx, 1       
    shr dx, 1       
    shr dx, 1       
    add dx, 10h     
    mov ax, 3100h
    int 21h
Init    ENDP
    END Init
