.model small
.stack 1000
.data
old dd 0 
counter db 0
.code
.486


new_handle proc        
    pusha       
    
    inc counter
    cmp counter, 20
    jnc endhandle
    
    mov ah, 02h
    mov dl, 'q'
    int 21h
    mov cl, 0
    mov couner, cl
    
    endhandle:
    mov al, 20h
    out 20h, al
    
    popa
    iret
new_handle endp
new_end:



main:
    mov ax, @data
    mov ds, ax
        
    cli
    pushf 
    push 0      
    pop ds
    mov eax, ds:[08h*4] 
    mov cs:[old], eax 
        
    mov ax, cs
    shl eax, 16
    mov ax, offset new_handle
    mov ds:[08h*4], eax
    sti 
        
    xor ax, ax
    mov ah, 31h
    MOV DX, (New_end - @code + 10FH) / 16 
    INT 21H 
end main
