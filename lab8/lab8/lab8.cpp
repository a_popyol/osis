﻿// lab8.cpp : Определяет точку входа для приложения.
//

#include "framework.h"
#include "lab8.h"

#define MAX_LOADSTRING 100

// Глобальные переменные:
HINSTANCE hInst;                                // текущий экземпляр
WCHAR szTitle[MAX_LOADSTRING];                  // Текст строки заголовка
WCHAR szWindowClass[MAX_LOADSTRING];            // имя класса главного окна

// Отправить объявления функций, включенных в этот модуль кода:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
void				InitControls(HWND);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Разместите код здесь.
	MSG msg;
	HACCEL hAccelTable;

	// Инициализация глобальных строк
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_LAB8, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Выполнить инициализацию приложения:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_LAB8));

	// Цикл основного сообщения:
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}



//
//  ФУНКЦИЯ: MyRegisterClass()
//
//  ЦЕЛЬ: Регистрирует класс окна.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_LAB8));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_LAB8);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   ФУНКЦИЯ: InitInstance(HINSTANCE, int)
//
//   ЦЕЛЬ: Сохраняет маркер экземпляра и создает главное окно
//
//   КОММЕНТАРИИ:
//
//        В этой функции маркер экземпляра сохраняется в глобальной переменной, а также
//        создается и выводится главное окно программы.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Сохранить маркер экземпляра в глобальной переменной

	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, 458, 879, nullptr, nullptr, hInstance, nullptr);

	InitControls(hWnd);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

#define BUTTON_DRAW_ID 1
#define BUTTON_CLEAR_ID 2

void InitControls(HWND hWnd) {
	HWND buttonAdd = CreateWindow(L"BUTTON", NULL, WS_VISIBLE | WS_CHILD | BS_OWNERDRAW | BS_PUSHBUTTON,
		20, 750, 60, 60, hWnd, (HMENU)BUTTON_DRAW_ID, NULL, NULL);
	HWND buttonClear = CreateWindow(L"BUTTON", NULL, WS_VISIBLE | WS_CHILD | BS_OWNERDRAW | BS_PUSHBUTTON,
		90, 750, 60, 60, hWnd, (HMENU)BUTTON_CLEAR_ID, NULL, NULL);
}

bool isVisible = false;

//
//  ФУНКЦИЯ: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  ЦЕЛЬ: Обрабатывает сообщения в главном окне.
//
//  WM_COMMAND  - обработать меню приложения
//  WM_PAINT    - Отрисовка главного окна
//  WM_DESTROY  - отправить сообщение о выходе и вернуться
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent, i;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Разобрать выбор в меню:
		switch (wmId)
		{
		case BUTTON_DRAW_ID:
			isVisible = true;
			InvalidateRect(hWnd, NULL, true);
			break;
		case BUTTON_CLEAR_ID:
			isVisible = false;
			InvalidateRect(hWnd, NULL, true);
			break;
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_DRAWITEM:
	{
		DRAWITEMSTRUCT* draw = (DRAWITEMSTRUCT*)lParam;
		HBRUSH brush;
		HPEN pen;

		switch (draw->CtlID)
		{
		case BUTTON_DRAW_ID:
		{
			HDC hdc = draw->hDC;

			brush = CreateSolidBrush(RGB(0, 255, 0));
			pen = CreatePen(BS_SOLID, 2, RGB(0, 0, 0));

			SelectObject(hdc, brush);
			SelectObject(hdc, pen);

			Ellipse(hdc, 0, 0, 60, 60);
		}
		break;
		case BUTTON_CLEAR_ID:
		{
			HDC hdc = draw->hDC;

			brush = CreateSolidBrush(RGB(255, 0, 0));

			pen = CreatePen(BS_SOLID, 2, RGB(0, 0, 0));

			SelectObject(hdc, brush);
			SelectObject(hdc, pen);

			Ellipse(hdc, 0, 0, 60, 60);
		}
		break;
		default:
			break;
		}

	}
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: Добавьте сюда любой код прорисовки, использующий HDC...

		HBRUSH brush;
		HPEN pen;
		if (isVisible)
		{
			brush = CreateSolidBrush(RGB(210, 210, 210));
			SelectObject(hdc, brush);
			pen = CreatePen(BS_SOLID, 1, RGB(210, 210, 210));
			SelectObject(hdc, pen);
			Rectangle(hdc, 0, 0, 1000, 1000);


			brush = CreateSolidBrush(RGB(239, 0, 0));
			SelectObject(hdc, brush);
			pen = CreatePen(BS_SOLID, 1, RGB(239, 0, 0));
			SelectObject(hdc, pen);
			POINT p1[9] = { 40, 199, 52, 452, 143, 679, 202, 559, 442, 869, 210, 492, 148, 585,
			96, 342, 101, 220 };
			Polygon(hdc, p1, 9);


			brush = CreateSolidBrush(RGB(1, 109, 197));
			SelectObject(hdc, brush);
			pen = CreatePen(BS_SOLID, 1, RGB(1, 109, 197));
			SelectObject(hdc, pen);
			Rectangle(hdc, 169, 221, 208, 495);
			Rectangle(hdc, 208, 221, 248, 343);

			pen = CreatePen(BS_SOLID, 25, RGB(1, 109, 197));
			SelectObject(hdc, pen);
			MoveToEx(hdc, 195, 465, NULL);
			LineTo(hdc, 254, 415);
			LineTo(hdc, 278, 526);
			pen = CreatePen(BS_SOLID, 12, RGB(1, 109, 197));
			SelectObject(hdc, pen);
			LineTo(hdc, 444, 855);



			brush = CreateSolidBrush(RGB(253, 224, 70));
			SelectObject(hdc, brush);
			pen = CreatePen(BS_SOLID, 1, RGB(253, 224, 70));
			SelectObject(hdc, pen);
			POINT p2[8] = { 167, 216, 185, 500, 238, 436, 442, 869, 210, 492, 148, 585,
			96, 342, 101, 220 };
			Polygon(hdc, p2, 8);


			brush = CreateSolidBrush(RGB(255, 255, 255));
			SelectObject(hdc, brush);
			pen = CreatePen(BS_SOLID, 10, RGB(0, 110, 198));
			SelectObject(hdc, pen);
			Ellipse(hdc, 31, 42, 193, 219);
			Ellipse(hdc, 130, 185, 217, 336);
			Ellipse(hdc, 229, 131, 418, 364);
			Ellipse(hdc, 146, 4, 312, 167);

			pen = CreatePen(BS_SOLID, 10, RGB(255, 255, 255));
			SelectObject(hdc, pen);
			Ellipse(hdc, 121, 97, 283, 256);


			pen = CreatePen(BS_SOLID, 10, RGB(0, 110, 198));
			SelectObject(hdc, pen);
			Arc(hdc, 133, 48, 193, 123, 145, 50, 186, 121);
		    Arc(hdc, 155, 108, 292, 239, 186, 121, 249, 195);

		}

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Обработчик сообщений для окна "О программе".
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
