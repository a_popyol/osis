#include <windows.h>
#include <string>
#include <stdio.h>
#include <initguid.h>
#include <virtdisk.h>
#include <winioctl.h>
#include <iostream>
#include <fstream>

using namespace std;

void PrintErrorMessage(ULONG ErrorId);
void GetInfo();

int main()
{
	setlocale(LC_ALL, "rus");

	string input = "           ";
	int dotPos = 0;
	//string input = "SCREEN  TXT";
	GetInfo();

	system("pause");
	return 0;
}

void GetInfo()
{
	const unsigned int SectorSize = 512;
	BOOL result;

	HANDLE hDisk = CreateFile("\\\\.\\G:", GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL); // disk pointer
	if (hDisk == NULL)
	{
		PrintErrorMessage(GetLastError());
		return;
	}
	BYTE rootTable[SectorSize];
	DWORD dwRead;
	result = ReadFile(hDisk, rootTable, sizeof(rootTable), &dwRead, NULL);
	if (!result || dwRead != sizeof(rootTable))
	{
		PrintErrorMessage(GetLastError());
		return;
	}
	for (int i = 0; i < 512; i++)
	{
		char f[3];
		_itoa(rootTable[i], f, 16);
	}

	int reservedSector = rootTable[15] * 256 + rootTable[14];
	int fatCount = rootTable[16];
	int sectorsPerCluster = rootTable[13];
	int rootEntries = rootTable[18] * 256 + rootTable[17];
	int fatSize = rootTable[23] * 256 + rootTable[22];

	int rootDirSecNum = fatCount * fatSize + reservedSector;

	SetFilePointer(hDisk, rootDirSecNum * SectorSize, NULL, FILE_BEGIN);

	BYTE rootDirBuffer[SectorSize];
	result = ReadFile(hDisk, rootDirBuffer, sizeof(rootDirBuffer), &dwRead, NULL);
	if (!result || dwRead != sizeof(rootDirBuffer))
	{
		PrintErrorMessage(GetLastError());
		return;
	}
	for (int i = 0; i < 512; i++)
	{
		char f[3];
		_itoa(rootDirBuffer[i], f, 16);
	}

	int pos = 96;
	int rootElemCount = 0;
	string filename = "no files";
	int maxClusters = 0;
	do
	{
		if (rootDirBuffer[pos] == 0 && rootDirBuffer[pos + 1] == 0)
		{
			break;
		}
		else
		{
			float size = rootDirBuffer[pos+28] + rootDirBuffer[pos + 29] * 256 + rootDirBuffer[pos + 30] * 256 * 256 + rootDirBuffer[pos + 31] * 256 * 256 * 256;
			float clusters = size / SectorSize / sectorsPerCluster;
			int clustersCount = ceil(clusters);
			if (clustersCount > maxClusters)
			{
				maxClusters = clustersCount;
				filename = "";
				for (int i = 0; i < 8; i++)
					filename += rootDirBuffer[pos + i];
				filename += '.';
				for (int i = 8; i < 11; i++)
					filename += rootDirBuffer[pos + i];

			}


			pos += 32;
			rootElemCount++;
			if (rootElemCount > rootEntries)
			{
				break;
			}
			if (pos >= SectorSize)
			{
				pos = 0;
				result = ReadFile(hDisk, rootDirBuffer, sizeof(rootDirBuffer), &dwRead, NULL);
				if (!result || dwRead != sizeof(rootDirBuffer))
				{
					PrintErrorMessage(GetLastError());
					return;
				}
				for (int i = 0; i < 512; i++)
				{
					char f[3];
					_itoa(rootDirBuffer[i], f, 16);
				}
			}

		}
	} while (true);
	cout << filename;
	CloseHandle(hDisk);
	return;
}

void PrintErrorMessage(ULONG ErrorId) //����� ��������� �� ������
{
	PVOID Message = NULL;
	FormatMessageW(FORMAT_MESSAGE_ALLOCATE_BUFFER |  //�������� ��������� �� ������ � Message
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		ErrorId,
		0,
		(LPWSTR)&Message,
		16,
		NULL);

	wprintf(L"%s\n", Message);
	LocalFree(Message);
}